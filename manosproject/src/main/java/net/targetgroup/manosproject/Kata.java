package net.targetgroup.manosproject;
/**
 * Class to implement accum function
 * @author redpower1989
 *
 */
public class Kata {
    
    private static final int LAST_CHARACTER = 1;
	private static final String DASH = "-";
	/**
	 * Method to take a string and return a String ("abcd") to the following format:
	 *  "A-Bb-Ccc-Dddd"
	 * @param s
	 * @return
	 */
	public static String accum(String s) {
    	StringBuilder resultBuilder = new StringBuilder();
    	for(int i=0; i<s.length(); i++){
    		char charAt = s.charAt(i);
			resultBuilder.append(String.valueOf(charAt).toUpperCase());
    		for(int j = 0; j < i; j++){
    			resultBuilder.append(String.valueOf(charAt).toLowerCase());
    		}
    		resultBuilder.append(DASH);
    	}
		resultBuilder.deleteCharAt(resultBuilder.length() - LAST_CHARACTER);
		return resultBuilder.toString();    
    }
}